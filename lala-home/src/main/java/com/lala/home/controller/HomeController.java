package com.lala.home.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lala.common.vo.ResponseResult;
import com.lala.home.pojo.entity.Banner;
import com.lala.home.pojo.entity.Category;
import com.lala.home.pojo.entity.Product;
import com.lala.home.sercive.BannerService;
import com.lala.home.sercive.CategoryService;
import com.lala.home.sercive.ProductService;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@Api(tags = "首页业务")
@RestController
@RequestMapping("/home")
public class HomeController {
    @Resource
    CategoryService categoryService;
    @Resource
    ProductService productService;
    @Resource
    BannerService bannerService;

    /**
     * 首页轮播图展示接口
     */
    @GetMapping("/bannerList")
    @ApiOperation("首页轮播图展示接口")
    public ResponseResult<List<Banner>> bannerList() {
        return ResponseResult.success(bannerService.getBannerList());
    }


    /**
     * 首页商品所有分类展示接口
     */
    @GetMapping("/allCategories")
    @ApiOperation("首页商品所有分类展示接口")
    public ResponseResult<List<Category>> allCategories() {
        return ResponseResult.success(categoryService.getAll());
    }

    /**
     * 查询商品子分类接口
     *
     * @param categoryId 商品分类id
     */
    @GetMapping("/categories")
    @ApiOperation("查询商品子分类接口")
    @ApiImplicitParam(value = "商品分类id", name = "categoryId", paramType = "query")
    public ResponseResult<List<Category>> categories(@RequestParam(name = "categoryId",
            defaultValue = "0") String categoryId) {
        return ResponseResult.success(categoryService.getCategories(categoryId));
    }

    /**
     * 分页查询某分类的商品
     *
     * @param categoryId 商品分类id
     */
    @GetMapping("/productList")
    @ApiOperation("分页查询某分类的商品")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "商品分类id", name = "categoryId", paramType = "query", required = true),
            @ApiImplicitParam(value = "当前页", name = "currentPage", paramType = "query"),
            @ApiImplicitParam(value = "每页显示数量", name = "size", paramType = "query"),
    })
    public ResponseResult<Page<Product>> productList(@RequestParam(value = "categoryId") String categoryId,
                                                     @RequestParam(value = "currentPage", defaultValue = "1") Integer currentPage,
                                                     @RequestParam(value = "size", defaultValue = "10") Integer size) {
        return ResponseResult.success(productService.getProductsByCategory(categoryId, currentPage, size));
    }

    /**
     * 按销量排序分页展示商品
     */
    @GetMapping("/show")
    @ApiOperation("按销量排序分页展示商品")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "当前页", name = "currentPage", paramType = "query"),
            @ApiImplicitParam(value = "每页显示数量", name = "size", paramType = "query"),
    })
    public ResponseResult<Page<Product>> show(@RequestParam(value = "currentPage", defaultValue = "1") Integer currentPage,
                                              @RequestParam(value = "size", defaultValue = "10") Integer size) {
        return ResponseResult.success(productService.getProducts(currentPage, size));
    }

}
