package com.lala.home.sercive;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lala.home.pojo.entity.Product;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 商品业务层接口
 *
 * @author yang
 */
public interface ProductService extends IService<Product> {

    /**
     * 查询某分类的商品(分页)
     *
     * @param categoryId  分类id
     * @param currentPage 当前页
     * @param size        每页显示数量
     * @return 商品集合
     */
    Page<Product> getProductsByCategory(String categoryId, Integer currentPage, Integer size);

    /**
     * @param currentPage 当前页
     * @param size        每页显示数量
     * @return 商品集合
     */
    Page<Product> getProducts(Integer currentPage, Integer size);
}
