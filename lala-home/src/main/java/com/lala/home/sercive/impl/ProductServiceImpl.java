package com.lala.home.sercive.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lala.home.mapper.PriceMapper;
import com.lala.home.pojo.entity.Price;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lala.home.mapper.ProductMapper;
import com.lala.home.pojo.entity.Product;
import com.lala.home.sercive.ProductService;

import javax.annotation.Resource;

@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements ProductService {
    @Resource
    ProductMapper productMapper;
    @Resource
    PriceMapper priceMapper;

    @Override
    public Page<Product> getProductsByCategory(String categoryId, Integer currentPage, Integer size) {
        QueryWrapper<Product> wrapper = new QueryWrapper<Product>()
                .eq(Product.CATEGORY_1ST, categoryId)
                .or()
                .eq(Product.CATEGORY_2ND, categoryId)
                .or()
                .eq(Product.CATEGORY_3RD, categoryId);
        //构造分页参数并调用page进行分页
        Page<Product> productPage = productMapper.selectPage(new Page<>(currentPage, size), wrapper);
        //查询每一个商品的价格
        productPage.getRecords().forEach(product -> {
            Price price = priceMapper.selectById(product.getProductId());
            product.setPrice(price);
        });
        return productPage;
    }

    @Override
    public Page<Product> getProducts(Integer currentPage, Integer size) {
        //条件构造器待写
        //QueryWrapper<Product> wrapper = new QueryWrapper<Product>();

        //构造分页参数并调用page进行分页
        Page<Product> productPage = productMapper.selectPage(new Page<>(currentPage, size), null);
        //查询每一个商品的价格
        productPage.getRecords().forEach(product -> {
            Price price = priceMapper.selectById(product.getProductId());
            product.setPrice(price);
        });
        return productPage;
    }
}
