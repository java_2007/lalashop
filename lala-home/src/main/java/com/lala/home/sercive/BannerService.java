package com.lala.home.sercive;

import com.lala.home.pojo.entity.Banner;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 轮播图业务层接口
 *
 * @author yang
 */
public interface BannerService extends IService<Banner> {
    /**
     * 获取首页轮播图列表
     * @return 轮播图集合
     */
    List<Banner> getBannerList();
}
