package com.lala.home.sercive.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lala.home.pojo.entity.Category;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lala.home.mapper.CategoriesMapper;
import com.lala.home.sercive.CategoryService;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CategoryServiceImpl extends ServiceImpl<CategoriesMapper, Category> implements CategoryService {
    @Resource
    CategoriesMapper categoriesMapper;

    @Override
    public List<Category> getAll() {
        //条件构造器：分类层级为1级
        QueryWrapper<Category> wrapper = new QueryWrapper<Category>()
                .eq(Category.CATEGORY_LEVEL, 1);

        //查询一级分类
        List<Category> categories1 = categoriesMapper.selectList(wrapper);
        categories1.forEach(category1 -> {

            //查询每一个一级分类下的子分类，即二级分类
            List<Category> categories2 = categoriesMapper.selectList(new QueryWrapper<Category>()
                    .eq(Category.PARENT_ID, category1.getCategoryId()));

            categories2.forEach(category2 -> {
                //查询每一个二级分类下的子分类，即三级分类
                List<Category> category3 = categoriesMapper.selectList(new QueryWrapper<Category>()
                        .eq(Category.PARENT_ID, category2.getCategoryId()));
                //将三级分类设置到其父分类
                category2.setCategoryList(category3);
            });

            //将二级分类设置到其父分类
            category1.setCategoryList(categories2);

        });
        return categories1;
    }

    @Override
    public List<Category> getCategories(String pid) {
        //条件构造器：根据父类id查询
        QueryWrapper<Category> wrapper = new QueryWrapper<Category>()
                .eq(Category.PARENT_ID, pid);
        return categoriesMapper.selectList(wrapper);
    }
}
