package com.lala.home.sercive.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lala.home.mapper.PriceMapper;
import com.lala.home.pojo.entity.Price;
import com.lala.home.sercive.PriceService;

@Service
public class PriceServiceImpl extends ServiceImpl<PriceMapper, Price> implements PriceService {

}
