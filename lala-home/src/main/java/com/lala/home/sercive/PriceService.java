package com.lala.home.sercive;

import com.lala.home.pojo.entity.Price;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 商品价格业务层接口
 * @author yang
 */
public interface PriceService extends IService<Price> {


}
