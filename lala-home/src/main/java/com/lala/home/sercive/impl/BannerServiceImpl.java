package com.lala.home.sercive.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lala.home.pojo.entity.Banner;
import com.lala.home.mapper.BannerMapper;
import com.lala.home.sercive.BannerService;

import javax.annotation.Resource;
import java.util.List;

@Service
public class BannerServiceImpl extends ServiceImpl<BannerMapper, Banner> implements BannerService {
    @Resource
    BannerMapper bannerMapper;

    @Override
    public List<Banner> getBannerList() {
        QueryWrapper<Banner> wrapper = new QueryWrapper<Banner>()
                .eq(Banner.STATUS, 1)
                .orderByAsc(Banner.PRIORITY);
        //构造分页参数
        Page<Banner> page = new Page<>(1, 5);
        //调用page进行分页查询前五条
        return bannerMapper.selectPage(page, wrapper).getRecords();
    }
}
