package com.lala.home.sercive;

import com.lala.home.pojo.entity.Category;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 商品分类业务层接口
 *
 * @author yang
 */
public interface CategoryService extends IService<Category> {
    /**
     * 查询所有树形分类
     *
     * @return 所有商品分类集合
     */
    List<Category> getAll();

    /**
     * 查询某分类的子分类
     * @param pid 父id
     * @return 商品子分类集合
     */
    List<Category> getCategories(String pid);
}
