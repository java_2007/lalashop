package com.lala.home.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lala.home.pojo.entity.Banner;

public interface BannerMapper extends BaseMapper<Banner> {
}