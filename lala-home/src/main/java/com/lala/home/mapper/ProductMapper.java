package com.lala.home.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lala.home.pojo.entity.Product;

public interface ProductMapper extends BaseMapper<Product> {
}