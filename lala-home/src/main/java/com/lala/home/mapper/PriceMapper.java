package com.lala.home.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lala.home.pojo.entity.Price;

/**
 * 商品价格数据层接口
 * @author yang
 */
public interface PriceMapper extends BaseMapper<Price> {
}