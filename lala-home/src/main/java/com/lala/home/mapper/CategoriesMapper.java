package com.lala.home.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lala.home.pojo.entity.Category;
import org.apache.ibatis.annotations.Mapper;

/***
 * 商品分类数据层接口
 * @author yang
 */
@Mapper
public interface CategoriesMapper extends BaseMapper<Category> {
}