package com.lala.home.pojo.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * 商品表示层对象
 */
@ApiModel(value = "商品实体类")
@Data
public class ProductVo {


}
