package com.lala.home.mapper;

import com.lala.home.HomeApplication;
import com.lala.home.pojo.entity.Category;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = HomeApplication.class)//项目启动类
public class CategoryMapperTest {

    @Resource
    CategoriesMapper categoriesMapper;

    @Test
    public void testGetCategory() {
        Category category = categoriesMapper.selectById(1);
        System.out.println(category);
    }
}
