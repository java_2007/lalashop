package com.lala.common.common;

import cn.hutool.core.io.file.FileNameUtil;
import lombok.Data;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpSession;
import java.util.UUID;

@Data
@Component
public class UploadFile {

    /*
    * fileName:  文件名
    * file:  文件拓展名
    * uploadPath:  userimg文件夹绝对路径
    * uuidFileName:  存放在服务器的文件名
    * absolutePath:  保存图片的绝对路径
    * savePath:  保存图片的相对路径
    * */
    public String filePath(HttpSession session, MultipartFile source){

        //获取文件名
        String fileName = source.getOriginalFilename();
        //获取文件拓展名
        String file = FileNameUtil.extName(fileName);

        String uploadPath = uploadPath(session);

        String uuidFileName = UUID.randomUUID().toString() + "." + file;

        String absolutePath = uploadPath + uuidFileName;

        String savePath = absolutePath.substring(absolutePath.indexOf("\\images"));

        return savePath;
    }

    public String uploadPath(HttpSession session){
        //获取upload文件位置
        String uploadPath = session.getServletContext().getRealPath("\\userimg");
        return uploadPath;
    }
}
