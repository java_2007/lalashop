package com.lala.common.common;


import com.lala.common.config.EmailRuleConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Random;

@Component
@Slf4j
public class EmailVerify {
    @Resource
    EmailRuleConfig emailRule;
    @Resource
    JavaMailSenderImpl javaMailSender;

    public boolean verify(HttpServletRequest request, String email){
        String code = (String) request.getSession().getAttribute("email");
        return email.equalsIgnoreCase(code);
    }

    public boolean email(HttpServletRequest request, String email){

        if(!(email.matches(emailRule.getRule()) && emailRule.getRule() != null)){
            return false;
        }

        String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder sb=new StringBuilder();
        for(int i=0;i < 6;i++)
        {
            char ch=str.charAt(new Random().nextInt(str.length()));
            sb.append(ch);
        }
        String authCode = sb + "";

        request.getSession().setAttribute("emailCode",authCode);

        SimpleMailMessage mailMessage = new SimpleMailMessage();

        //需要发送的邮箱
        mailMessage.setTo(email);
        //发送者的邮箱
        mailMessage.setFrom(emailRule.getEmail());
        mailMessage.setReplyTo("test");
        mailMessage.setSentDate(new Date());
        //邮箱标题
        mailMessage.setSubject("邮箱验证");
        //邮箱内容
        mailMessage.setText("你的拉拉商城邮箱验证码是" + authCode);
        javaMailSender.send(mailMessage);

        log.info("发送成功");

        return true;
    }
}
