package com.lala.common;

import com.lala.common.common.EmailVerify;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication(scanBasePackages = "com.lala.common")
public class CommonApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(CommonApplication.class, args);
        EmailVerify emailVerify = (EmailVerify) run.getBean("emailVerify");
        System.out.println(emailVerify);
    }
}
