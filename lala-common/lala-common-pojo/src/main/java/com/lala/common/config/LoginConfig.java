package com.lala.common.config;

import com.lala.common.common.LoginHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

@Configuration
public class LoginConfig implements WebMvcConfigurer {
    @Resource
    LoginHandler loginHandler;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginHandler).addPathPatterns("/email/**","/upload/**");
        //排除不需要拦截的
        /*registry.addInterceptor(loginHandler)
                .addPathPatterns("/**")
                .excludePathPatterns("/css/**","/js/**","/img/**");*/
    }
}
