package com.lala.commodity.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lala.commodity.entity.Price;

public interface PriceMapper extends BaseMapper<Price> {
}