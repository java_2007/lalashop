package com.lala.commodity.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lala.commodity.common.dto.PagingDto;
import com.lala.commodity.common.vo.SeekVo;
import com.lala.commodity.service.SeekService;
import com.lala.common.vo.ResponseResult;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

@RestController
public class CommodityController {
    @Resource
    SeekService seekService;

    @PostMapping("/seek")
    public ResponseResult<List<SeekVo>> seek(@RequestBody PagingDto pagingDto){

        return seekService.seekCommodity(pagingDto);
    }

    @PostMapping("/test")
    public ResponseResult<IPage<SeekVo>> page(@RequestBody PagingDto pagingDto){

        return seekService.pageCommodity(pagingDto);
    }
}
