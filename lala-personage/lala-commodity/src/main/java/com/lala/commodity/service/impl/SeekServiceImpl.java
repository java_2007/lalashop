package com.lala.commodity.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lala.commodity.common.dto.PagingDto;
import com.lala.commodity.common.vo.SeekVo;
import com.lala.commodity.mapper.ProductMapper;
import com.lala.commodity.service.SeekService;
import com.lala.common.exception.ServiceException;
import com.lala.common.vo.ResponseCode;
import com.lala.common.vo.ResponseResult;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

@Service
public class SeekServiceImpl implements SeekService {

    @Resource
    ProductMapper productMapper;

    @Override
    public ResponseResult<List<SeekVo>> seekCommodity(PagingDto pagingDto) {
        Integer count = productMapper.selectCount(pagingDto.getName());

        if(pagingDto.getPage() < 1 ||
                pagingDto.getSize() < 1 ||
                pagingDto.getPage() * pagingDto.getSize() > (count+ pagingDto.getSize())){
            pagingDto.setPage(1);
            pagingDto.setSize(10);
        }
        System.out.println(pagingDto.getPage());
        pagingDto.setPage((pagingDto.getPage()-1)* pagingDto.getSize());
        System.out.println(pagingDto.getPage());
        List<SeekVo> seekVo = productMapper.selectProduct(pagingDto);

        if (seekVo.size() == 0){
            throw new ServiceException(ResponseCode.SQL_ERROR);
        }

        return ResponseResult.success(seekVo);
    }

    @Override
    public ResponseResult<IPage<SeekVo>> pageCommodity(PagingDto pagingDto) {
        if(pagingDto.getPage() < 1 || pagingDto.getSize() < 1){
            pagingDto.setPage(1);
            pagingDto.setSize(10);
        }
        Page<SeekVo> seekVoPage = new Page<>(pagingDto.getPage(), pagingDto.getSize());
        IPage<SeekVo> seekVoIPage = productMapper.selectPage(pagingDto.getName(),seekVoPage);
        return ResponseResult.success(seekVoIPage);
    }
}
