package com.lala.commodity.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lala.commodity.common.dto.PagingDto;
import com.lala.commodity.common.vo.SeekVo;
import com.lala.common.vo.ResponseResult;
import java.util.List;

public interface SeekService {

    ResponseResult<List<SeekVo>> seekCommodity(PagingDto pagingDto);

    ResponseResult<IPage<SeekVo>> pageCommodity(PagingDto pagingDto);
}
