package com.lala.commodity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
    * 商品价格表
    */
@ApiModel(value="com-lala-commodity-entity-Price")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "price")
public class Price implements Serializable {
    /**
     * 商品id
     */
    @TableId(value = "product_id", type = IdType.INPUT)
    @ApiModelProperty(value="商品id")
    private String productId;

    /**
     * 商品原价,单位-元保留两位小数
     */
    @TableField(value = "original_price")
    @ApiModelProperty(value="商品原价,单位-元保留两位小数")
    private BigDecimal originalPrice;

    /**
     * 商品促销价,单位-元保留两位小数
     */
    @TableField(value = "promote_price")
    @ApiModelProperty(value="商品促销价,单位-元保留两位小数")
    private BigDecimal promotePrice;

    /**
     * 商品成本价,单位-元保留两位小数
     */
    @TableField(value = "cost_price")
    @ApiModelProperty(value="商品成本价,单位-元保留两位小数")
    private BigDecimal costPrice;

    /**
     * 逻辑删除(1、删除，0、未删除)
     */
    @TableField(value = "is_delete")
    @ApiModelProperty(value="逻辑删除(1、删除，0、未删除)")
    private Integer isDelete;

    private static final long serialVersionUID = 1L;

    public static final String COL_PRODUCT_ID = "product_id";

    public static final String COL_ORIGINAL_PRICE = "original_price";

    public static final String COL_PROMOTE_PRICE = "promote_price";

    public static final String COL_COST_PRICE = "cost_price";

    public static final String COL_IS_DELETE = "is_delete";
}