package com.lala.commodity.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
@ConfigurationProperties(prefix = "paging.dto")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PagingDto implements Serializable {

    private String name;
    private Integer page;
    private Integer size;
}
