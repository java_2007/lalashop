package com.lala.commodity.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lala.commodity.common.dto.PagingDto;
import com.lala.commodity.common.vo.SeekVo;
import com.lala.commodity.entity.Product;
import org.apache.ibatis.annotations.Param;
import java.util.List;

public interface ProductMapper extends BaseMapper<Product> {

    List<SeekVo> selectProduct(@Param("pagingDto") PagingDto pagingDto);

    Integer selectCount(@Param("name") String name);

    IPage<SeekVo> selectPage(String name, Page<SeekVo> page);
}