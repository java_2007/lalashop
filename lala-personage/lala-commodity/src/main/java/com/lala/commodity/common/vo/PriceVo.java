package com.lala.commodity.common.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
@Data
public class PriceVo implements Serializable {
    /**
     * 商品原价,单位-元保留两位小数
     */
    @TableField(value = "original_price")
    @ApiModelProperty(value="商品原价,单位-元保留两位小数")
    private BigDecimal originalPrice;

    /**
     * 商品促销价,单位-元保留两位小数
     */
    @TableField(value = "promote_price")
    @ApiModelProperty(value="商品促销价,单位-元保留两位小数")
    private BigDecimal promotePrice;
}
