package com.lala.commodity.common.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class SeekVo implements Serializable {
    /**
     * 商品id
     */
    @TableId(value = "product_id", type = IdType.INPUT)
    @ApiModelProperty(value = "商品id")
    private String productId;

    /**
     * 一级分类id
     */
    @TableField(value = "category_1st")
    @ApiModelProperty(value = "一级分类id")
    private String category1st;

    /**
     * 二级分类id
     */
    @TableField(value = "category_2nd")
    @ApiModelProperty(value = "二级分类id")
    private String category2nd;

    /**
     * 三级分类id
     */
    @TableField(value = "category_3rd")
    @ApiModelProperty(value = "三级分类id")
    private String category3rd;

    /**
     * 商品名称
     */
    @TableField(value = "product_name")
    @ApiModelProperty(value = "商品名称")
    private String productName;

    /**
     * 商品副标题
     */
    @TableField(value = "subtitle")
    @ApiModelProperty(value = "商品副标题")
    private String subtitle;

    /**
     * 产品主图,url相对地址
     */
    @TableField(value = "image_url")
    @ApiModelProperty(value = "产品主图,url相对地址")
    private String imageUrl;

    /**
     * 商品详情
     */
    @TableField(value = "detail")
    @ApiModelProperty(value = "商品详情")
    private String detail;

    /**
     * 库存数量
     */
    @TableField(value = "stock")
    @ApiModelProperty(value = "库存数量")
    private Long stock;

    /**
     * 商品销量
     */
    @TableField(value = "sales_num")
    @ApiModelProperty(value = "商品销量")
    private Integer salesNum;

    /**
     * 价格
     */
    @TableField(value = "stock")
    @ApiModelProperty(value = "价格")
    private PriceVo priceVo;


}
