package com.lala.information.mapper;

import com.lala.information.entity.UserInformation;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
public class UserInformationMapperTest {
    @Resource
    UserInformationMapper userInformationMapper;

    @Test
    public void testSelectById() {
        UserInformation userInformation = userInformationMapper.selectByUserId("1");
        System.out.println(userInformation);
    }
}
