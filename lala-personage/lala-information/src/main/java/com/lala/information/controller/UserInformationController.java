package com.lala.information.controller;

import com.lala.common.vo.ResponseCode;
import com.lala.common.vo.ResponseResult;
import com.lala.information.common.dto.UserDto;
import com.lala.information.service.UserInformationService;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;

@RestController
@RequestMapping("/upload")
public class UserInformationController {
    @Resource
    UserInformationService userInformationService;

    @PostMapping("/userinformation")
    public ResponseResult<ResponseCode> upload(@ApiIgnore HttpSession session,
                               @RequestBody UserDto userDto) throws IOException, ParseException {
        return userInformationService.upload(session, userDto);
    }

}
