package com.lala.information.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lala.information.entity.Address;
import com.lala.information.entity.UserInformation;
import org.apache.ibatis.annotations.Param;

/**
 * 用户详细信息数据层接口
 */
public interface UserInformationMapper extends BaseMapper<UserInformation> {
    /**
     * 根据用户id查询用户详细信息
     *
     * @return 用户详细信息实体类
     */
    UserInformation selectByUserId(@Param("userId") String userId);

    Integer updateUserInformation(@Param("user") UserInformation user,
                                  @Param("address")Address address);
}