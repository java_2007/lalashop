package com.lala.information.service.impl;

import com.lala.common.common.UploadFile;
import com.lala.common.vo.ResponseCode;
import com.lala.common.vo.ResponseResult;
import com.lala.information.common.dto.UserDto;
import com.lala.information.entity.Address;
import com.lala.information.entity.UserInformation;
import com.lala.information.mapper.UserInformationMapper;
import com.lala.information.service.UserInformationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class UserInformationServiceImpl implements UserInformationService {
    @Resource
    UserInformationMapper userInformationMapper;
    @Resource
    UploadFile uploadFile;

    @Override
    @Transactional
    public ResponseResult<ResponseCode> upload(HttpSession session, UserDto userDto) throws IOException, ParseException {
        UserInformation userInformation = new UserInformation();
        if(userDto.getUserPic()!=null){
            //获取userimg文件夹的绝对路径
            String uploadPath = uploadFile.uploadPath(session);
            //将文件存储到userimg文件夹下
            userDto.getUserPic().transferTo(new File(uploadPath));
            //获取文件的相对存储路路径
            String filePath = uploadFile.filePath(session, userDto.getUserPic());
            userInformation.setUserPic(filePath);
        }
        userInformation.setSex(userDto.getSex());

        String id = (String) session.getAttribute("id");

        SimpleDateFormat sdf = new SimpleDateFormat();
        Date parse = sdf.parse(sdf.format(new Date()));

        Address address = new Address();
        address.setUserId(id);
        address.setAddressId(userDto.getUserInformationDto().getAddressId());
        address.setDetail(userDto.getUserInformationDto().getDetail());
        address.setReceiverName(userDto.getUserInformationDto().getReceiverName());
        address.setReceiverPhone(userDto.getUserInformationDto().getReceiverPhone());
        address.setUpdateTime(parse);
        Integer result = userInformationMapper.updateUserInformation(userInformation, address);

        if(result==1){
            return ResponseResult.success(ResponseCode.SUCCESS);
        }
        return ResponseResult.success(ResponseCode.SQL_ERROR);
    }
}
