package com.lala.information.controller;

import com.lala.common.common.EmailVerify;
import com.lala.common.vo.ResponseCode;
import com.lala.common.vo.ResponseResult;
import com.lala.information.common.dto.EmailDto;
import com.lala.information.service.EmailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Api(tags = "绑定邮箱")
@RestController
@RequestMapping("/email")
public class EmailController {

    @Resource
    EmailService emailService;

    @PostMapping("/")
    @ApiOperation("发送邮箱验证码接口")
    public ResponseResult<ResponseCode> send(HttpServletRequest request,@RequestBody EmailDto emailDto){

        return emailService.send(request, emailDto);
    }

    @PostMapping("/binding")
    @ApiOperation("验证验证码接口")
    public ResponseResult<ResponseCode> binding(HttpServletRequest request,@RequestBody EmailDto emailDto){

        return emailService.binding(request,emailDto);
    }
}
