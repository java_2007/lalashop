package com.lala.information.service;

import com.lala.common.vo.ResponseCode;
import com.lala.common.vo.ResponseResult;
import com.lala.information.common.dto.UserDto;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;

public interface UserInformationService {

    ResponseResult<ResponseCode> upload(HttpSession session, UserDto userDto) throws IOException, ParseException;

}
