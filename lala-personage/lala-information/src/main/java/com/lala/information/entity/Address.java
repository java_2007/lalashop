package com.lala.information.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户收货地址表
 */
@ApiModel(value = "com-lala-information-entity-Address")
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "address")
public class Address implements Serializable {
    /**
     * 地址编号
     */
    @TableId(value = "address_id", type = IdType.INPUT)
    @ApiModelProperty(value = "地址编号")
    private String addressId;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value = "用户id")
    private String userId;

    /**
     * 详细地址
     */
    @TableField(value = "detail")
    @ApiModelProperty(value = "详细地址")
    private String detail;

    /**
     * 收件人
     */
    @TableField(value = "receiver_name")
    @ApiModelProperty(value = "收件人")
    private String receiverName;

    /**
     * 收件人手机号
     */
    @TableField(value = "receiver_phone")
    @ApiModelProperty(value = "收件人手机号")
    private Long receiverPhone;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    /**
     * 逻辑删除(1、删除，0、未删除)
     */
    @TableField(value = "is_delete")
    @ApiModelProperty(value = "逻辑删除(1、删除，0、未删除)")
    private Integer isDelete;

    private static final long serialVersionUID = 1L;

    public static final String COL_ADDRESS_ID = "address_id";

    public static final String COL_USER_ID = "user_id";

    public static final String COL_DETAIL = "detail";

    public static final String COL_RECEIVER_NAME = "receiver_name";

    public static final String COL_RECEIVER_PHONE = "receiver_phone";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_TIME = "update_time";

    public static final String COL_IS_DELETE = "is_delete";
}