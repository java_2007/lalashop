package com.lala.information.common.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class EmailDto implements Serializable {
    private String email;
    private String code;
    private String uid;
}
