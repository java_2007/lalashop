package com.lala.information.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@ApiModel(value = "com-lala-information-entity-UserInformation")
@Data
public class UserInformation {
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID")
    private String userId;

    /**
     * 用户性别
     */
    @ApiModelProperty(value = "用户性别")
    private String sex;

    /**
     * 用户邮箱
     */
    @ApiModelProperty(value = "用户邮箱")
    private String email;

    /**
     * 用户电话
     */
    @ApiModelProperty(value = "用户电话")
    private Long iphone;

    /**
     * 用户头像的地址
     */
    @ApiModelProperty(value = "用户头像的地址")
    private String userPic;

    /**
     * vip用户 0为普通 1为vip 2为svip
     */
    @ApiModelProperty(value = "用户会员")
    private String vip;

    /**
     * 用户收获地址
     */
    private List<Address> addressList;

}