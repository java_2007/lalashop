package com.lala.information.service;

import com.lala.common.vo.ResponseCode;
import com.lala.common.vo.ResponseResult;
import com.lala.information.common.dto.EmailDto;

import javax.servlet.http.HttpServletRequest;

public interface EmailService {

    ResponseResult<ResponseCode> send(HttpServletRequest request, EmailDto emailDto);

    ResponseResult<ResponseCode> binding(HttpServletRequest request, EmailDto code);

}
