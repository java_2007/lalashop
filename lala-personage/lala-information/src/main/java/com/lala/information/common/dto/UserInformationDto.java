package com.lala.information.common.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

@ApiModel(value="com-lala-information-entity-UserInformationDto")
@Data
public class UserInformationDto implements Serializable {

    /**
     * 地址编号
     */
    @ApiModelProperty(value="地址编号")
    private String addressId;

    /**
     * 详细地址
     */
    @ApiModelProperty(value="详细地址")
    private String detail;

    /**
     * 收件人
     */
    @ApiModelProperty(value="收件人")
    private String receiverName;

    /**
     * 收件人手机号
     */
    @ApiModelProperty(value="收件人手机号")
    private Long receiverPhone;
}
