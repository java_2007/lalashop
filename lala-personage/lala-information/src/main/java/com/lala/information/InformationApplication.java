package com.lala.information;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.lala")
public class InformationApplication {
    public static void main(String[] args) {
        SpringApplication.run(InformationApplication.class, args);
    }
}
