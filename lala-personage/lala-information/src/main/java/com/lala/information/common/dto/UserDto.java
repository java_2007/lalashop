package com.lala.information.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;
import java.io.Serializable;

@Data
public class UserDto implements Serializable {
    private MultipartFile userPic;
    /**
     * 用户性别
     */
    @ApiModelProperty(value="用户性别")
    private String sex;

    @ApiModelProperty(value="用户性别")
    private UserInformationDto userInformationDto;
}
