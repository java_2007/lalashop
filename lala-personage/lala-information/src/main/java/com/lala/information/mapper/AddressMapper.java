package com.lala.information.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lala.information.entity.Address;

public interface AddressMapper extends BaseMapper<Address> {
}