package com.lala.information.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.injector.methods.SelectPage;
import com.lala.common.common.EmailVerify;
import com.lala.common.vo.ResponseCode;
import com.lala.common.vo.ResponseResult;
import com.lala.information.common.dto.EmailDto;
import com.lala.information.entity.UserInformation;
import com.lala.information.mapper.UserInformationMapper;
import com.lala.information.service.EmailService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Service
public class EmailSerivceImpl implements EmailService {
    @Resource
    EmailVerify emailVerify;
    @Resource
    UserInformationMapper informationMapper;

    @Override
    public ResponseResult send(HttpServletRequest request, EmailDto emailDto) {
        String uid = (String)request.getSession().getAttribute("uid");
        if(uid == null){
            return ResponseResult.success(ResponseCode.NOT_LOGIN);
        }
        request.getSession().setAttribute("email",emailDto.getEmail());
        return ResponseResult.success(ResponseCode.SUCCESS,emailVerify.email(request, emailDto.getEmail()));
    }

    @Override
    public ResponseResult<ResponseCode> binding(HttpServletRequest request,  EmailDto code) {
        String emailCode = (String) request.getSession().getAttribute("emailCode");
        String emailNum = (String) request.getSession().getAttribute("email");
        if(!(emailCode.equalsIgnoreCase(code.getCode()) && code.getCode() != null)){
            return ResponseResult.error(ResponseCode.ERROR);
        }
        UpdateWrapper<UserInformation> userWrapper = new UpdateWrapper<>();

        UserInformation userInformation = new UserInformation();
        userInformation.setEmail(emailNum);
        userInformation.setUserId(code.getUid());
        userWrapper.eq("user_id",code.getUid());
        Integer result = informationMapper.update(userInformation, userWrapper);
        if(result == 1){
            return ResponseResult.success(ResponseCode.SUCCESS);
        }
        return ResponseResult.success(ResponseCode.ERROR);
    }
}
