package com.lala.admin.service;

import com.lala.admin.pojo.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 用户订单管理业务层
 *
 * @author yang
 */
public interface OrderService extends IService<Order> {


}
