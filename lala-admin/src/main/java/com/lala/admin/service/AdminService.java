package com.lala.admin.service;

import com.lala.admin.pojo.entity.Admin;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 管理员业务层
 *
 * @author yang
 */
public interface AdminService extends IService<Admin> {


}



