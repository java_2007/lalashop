package com.lala.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lala.admin.mapper.AdminMapper;
import org.springframework.stereotype.Service;

import com.lala.admin.pojo.entity.Admin;
import com.lala.admin.service.AdminService;


@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService {


}



