package com.lala.admin.service;

import com.lala.admin.pojo.entity.Price;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 商品价格管理业务层
 *
 * @author yang
 */
public interface PriceService extends IService<Price> {


}
