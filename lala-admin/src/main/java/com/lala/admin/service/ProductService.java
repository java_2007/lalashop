package com.lala.admin.service;

import com.lala.admin.pojo.entity.Product;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 商品管理业务层
 *
 * @author yang
 */
public interface ProductService extends IService<Product> {


}
