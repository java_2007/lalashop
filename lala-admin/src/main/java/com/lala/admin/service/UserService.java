package com.lala.admin.service;

import com.lala.admin.pojo.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 用户管理业务层
 *
 * @author yang
 */
public interface UserService extends IService<User> {


}
