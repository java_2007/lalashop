package com.lala.admin.service;

import com.lala.admin.pojo.entity.Cart;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 用户购物车业务层
 *
 * @author yang
 */
public interface CartService extends IService<Cart> {


}
