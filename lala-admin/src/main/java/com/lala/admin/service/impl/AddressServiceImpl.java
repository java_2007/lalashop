package com.lala.admin.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lala.admin.mapper.AddressMapper;
import com.lala.admin.pojo.entity.Address;
import com.lala.admin.service.AddressService;

@Service
public class AddressServiceImpl extends ServiceImpl<AddressMapper, Address> implements AddressService {

}
