package com.lala.admin.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lala.admin.pojo.entity.UserInformation;
import com.lala.admin.mapper.UserInformationMapper;
import com.lala.admin.service.UserInformationService;

@Service
public class UserInformationServiceImpl extends ServiceImpl<UserInformationMapper, UserInformation> implements UserInformationService {

}
