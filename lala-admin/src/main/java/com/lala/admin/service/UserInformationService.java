package com.lala.admin.service;

import com.lala.admin.pojo.entity.UserInformation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 用户详细信息业务层
 *
 * @author yang
 */
public interface UserInformationService extends IService<UserInformation> {


}
