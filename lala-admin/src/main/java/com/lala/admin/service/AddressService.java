package com.lala.admin.service;

import com.lala.admin.pojo.entity.Address;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 用户收货地址业务层
 *
 * @author yang
 */
public interface AddressService extends IService<Address> {


}
