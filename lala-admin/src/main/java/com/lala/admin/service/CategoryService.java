package com.lala.admin.service;

import com.lala.admin.pojo.entity.Category;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 商品分类管理业务层
 *
 * @author yang
 */
public interface CategoryService extends IService<Category> {


}
