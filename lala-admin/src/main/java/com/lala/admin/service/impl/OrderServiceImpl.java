package com.lala.admin.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lala.admin.mapper.OrderMapper;
import com.lala.admin.pojo.entity.Order;
import com.lala.admin.service.OrderService;

@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

}
