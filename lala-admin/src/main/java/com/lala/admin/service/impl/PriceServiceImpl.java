package com.lala.admin.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lala.admin.mapper.PriceMapper;
import com.lala.admin.pojo.entity.Price;
import com.lala.admin.service.PriceService;

@Service
public class PriceServiceImpl extends ServiceImpl<PriceMapper, Price> implements PriceService {

}
