package com.lala.admin.service;

import com.lala.admin.pojo.entity.Banner;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 轮播图管理业务层
 *
 * @author yang
 */
public interface BannerService extends IService<Banner> {


}
