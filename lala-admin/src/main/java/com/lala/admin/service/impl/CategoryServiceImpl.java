package com.lala.admin.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lala.admin.pojo.entity.Category;
import com.lala.admin.mapper.CategoryMapper;
import com.lala.admin.service.CategoryService;

@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

}
