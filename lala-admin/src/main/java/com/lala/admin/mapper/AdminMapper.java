package com.lala.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lala.admin.pojo.entity.Admin;

public interface AdminMapper extends BaseMapper<Admin> {
}