package com.lala.admin.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 商品分类实体类
 */
@ApiModel(value = "商品分类实体类")
@Data
@TableName(value = "category")
public class Category implements Serializable {
    /**
     * 商品分类id
     */
    @TableId(value = "category_id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "商品分类id")
    private String categoryId;

    /**
     * 分类名
     */
    @TableField(value = "category_name")
    @ApiModelProperty(value = "分类名")
    private String categoryName;

    /**
     * 层级
     */
    @TableField(value = "category_level")
    @ApiModelProperty(value = "层级")
    private Byte categoryLevel;

    /**
     * 父ID
     */
    @TableField(value = "parent_id")
    @ApiModelProperty(value = "父ID")
    private String parentId;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    /**
     * 逻辑删除(1、删除，0、未删除)
     */
    @TableField(value = "is_delete")
    @ApiModelProperty(value = "逻辑删除(1、删除，0、未删除)")
    private Byte isDelete;

    private static final long serialVersionUID = 1L;

    public static final String CATEGORY_ID = "category_id";
    public static final String CATEGORY_NAME = "category_name";
    public static final String CATEGORY_LEVEL = "category_level";
    public static final String PARENT_ID = "parent_id";
    public static final String CREATE_TIME = "create_time";
    public static final String UPDATE_TIME = "update_time";
    public static final String IS_DELETE = "is_delete";
}