package com.lala.admin.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

/**
 * 订单实体类
 */
@ApiModel(value = "订单实体类")
@Data
@TableName(value = "`order`")
public class Order implements Serializable {
    /**
     * 订单编号
     */
    @TableId(value = "order_id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "订单编号")
    private String orderId;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value = "用户id")
    private String userId;

    /**
     * 订单总金额
     */
    @TableField(value = "money")
    @ApiModelProperty(value = "订单总金额")
    private BigDecimal money;

    /**
     * 收货地址id
     */
    @TableField(value = "address_id")
    @ApiModelProperty(value = "收货地址id")
    private String addressId;

    /**
     * 订单状态：0未支付，1支付失败，2支付成功,3发货，4到货，5评价
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value = "订单状态：0未支付，1支付失败，2支付成功,3发货，4到货，5评价")
    private Integer status;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    /**
     * 逻辑删除(1、删除，0、未删除)
     */
    @TableField(value = "is_delete")
    @ApiModelProperty(value = "逻辑删除(1、删除，0、未删除)")
    private Integer isDelete;

    private static final long serialVersionUID = 1L;

    public static final String COL_ORDER_ID = "order_id";
    public static final String COL_USER_ID = "user_id";
    public static final String COL_MONEY = "money";
    public static final String COL_ADDRESS_ID = "address_id";
    public static final String COL_STATUS = "status";
    public static final String COL_CREATE_TIME = "create_time";
    public static final String COL_UPDATE_TIME = "update_time";
    public static final String COL_IS_DELETE = "is_delete";
}