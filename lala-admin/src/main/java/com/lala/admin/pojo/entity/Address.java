package com.lala.admin.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 用户收货地址实体类
 */
@ApiModel(value = "用户收货地址实体类")
@Data
@TableName(value = "address")
public class Address implements Serializable {
    /**
     * 用户地址id
     */
    @TableId(value = "address_id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "用户地址id")
    private String addressId;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value = "用户id")
    private String userId;

    /**
     * 详细地址
     */
    @TableField(value = "detail")
    @ApiModelProperty(value = "详细地址")
    private String detail;

    /**
     * 收件人
     */
    @TableField(value = "receiver_name")
    @ApiModelProperty(value = "收件人")
    private String receiverName;

    /**
     * 收件人手机号
     */
    @TableField(value = "receiver_phone")
    @ApiModelProperty(value = "收件人手机号")
    private Long receiverPhone;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    /**
     * 逻辑删除(1、删除，0、未删除)
     */
    @TableField(value = "is_delete")
    @ApiModelProperty(value = "逻辑删除(1、删除，0、未删除)")
    private Integer isDelete;

    private static final long serialVersionUID = 1L;

    public static final String ADDRESS_ID = "address_id";
    public static final String USER_ID = "user_id";
    public static final String DETAIL = "detail";
    public static final String RECEIVER_NAME = "receiver_name";
    public static final String RECEIVER_PHONE = "receiver_phone";
    public static final String CREATE_TIME = "create_time";
    public static final String UPDATE_TIME = "update_time";
    public static final String IS_DELETE = "is_delete";
}