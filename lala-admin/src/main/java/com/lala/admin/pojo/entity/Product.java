package com.lala.admin.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 商品实体类
 */
@ApiModel(value = "商品实体类")
@Data
@TableName(value = "product")
public class Product implements Serializable {
    /**
     * 商品id
     */
    @TableId(value = "product_id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "商品id")
    private String productId;

    /**
     * 一级分类id
     */
    @TableField(value = "category_1st")
    @ApiModelProperty(value = "一级分类id")
    private String category1st;

    /**
     * 二级分类id
     */
    @TableField(value = "category_2nd")
    @ApiModelProperty(value = "二级分类id")
    private String category2nd;

    /**
     * 三级分类id
     */
    @TableField(value = "category_3rd")
    @ApiModelProperty(value = "三级分类id")
    private String category3rd;

    /**
     * 商品名称
     */
    @TableField(value = "product_name")
    @ApiModelProperty(value = "商品名称")
    private String productName;

    /**
     * 商品副标题
     */
    @TableField(value = "subtitle")
    @ApiModelProperty(value = "商品副标题")
    private String subtitle;

    /**
     * 产品主图,url相对地址
     */
    @TableField(value = "image_url")
    @ApiModelProperty(value = "产品主图,url相对地址")
    private String imageUrl;

    /**
     * 商品详情
     */
    @TableField(value = "detail")
    @ApiModelProperty(value = "商品详情")
    private String detail;

    /**
     * 库存数量
     */
    @TableField(value = "stock")
    @ApiModelProperty(value = "库存数量")
    private Long stock;

    /**
     * 商品状态.1-在售 2-下架
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value = "商品状态.1-在售 2-下架")
    private Integer status;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    /**
     * 逻辑删除(1、删除，0、未删除)
     */
    @TableField(value = "is_delete")
    @ApiModelProperty(value = "逻辑删除(1、删除，0、未删除)")
    private Integer isDelete;

    private static final long serialVersionUID = 1L;

    public static final String PRODUCT_ID = "product_id";
    public static final String CATEGORY_1ST = "category_1st";
    public static final String CATEGORY_2ND = "category_2nd";
    public static final String CATEGORY_3RD = "category_3rd";
    public static final String PRODUCT_NAME = "product_name";
    public static final String SUBTITLE = "subtitle";
    public static final String IMAGE_URL = "image_url";
    public static final String DETAIL = "detail";
    public static final String STOCK = "stock";
    public static final String STATUS = "status";
    public static final String CREATE_TIME = "create_time";
    public static final String UPDATE_TIME = "update_time";
    public static final String IS_DELETE = "is_delete";
}