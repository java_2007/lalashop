package com.lala.admin.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 轮播图实体类
 */
@ApiModel(value = "轮播图实体类")
@Data
@TableName(value = "banner")
public class Banner implements Serializable {
    /**
     * 轮播图id
     */
    @TableId(value = "banner_id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "轮播图id")
    private String bannerId;

    /**
     * 标题
     */
    @TableField(value = "title")
    @ApiModelProperty(value = "标题")
    private String title;

    /**
     * 轮播图路径
     */
    @TableField(value = "image_url")
    @ApiModelProperty(value = "轮播图路径")
    private String imageUrl;

    /**
     * 详情路径
     */
    @TableField(value = "detail_url")
    @ApiModelProperty(value = "详情路径")
    private String detailUrl;

    /**
     * 优先级(显示顺序)
     */
    @TableField(value = "priority")
    @ApiModelProperty(value = "优先级(显示顺序)")
    private Integer priority;

    /**
     * 是否显示(1、显示，0、不显示)
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value = "是否显示(1、显示，0、不显示)")
    private Byte status;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    /**
     * 逻辑删除(1、删除，0、未删除)
     */
    @TableField(value = "is_delete")
    @ApiModelProperty(value = "逻辑删除(1、删除，0、未删除)")
    private Byte isDelete;

    private static final long serialVersionUID = 1L;

    public static final String BANNER_ID = "banner_id";
    public static final String TITLE = "title";
    public static final String IMAGE_URL = "image_url";
    public static final String DETAIL_URL = "detail_url";
    public static final String PRIORITY = "priority";
    public static final String STATUS = "status";
    public static final String CREATE_TIME = "create_time";
    public static final String UPDATE_TIME = "update_time";
    public static final String IS_DELETE = "is_delete";
}