package com.lala.admin.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

/**
 * 商品价格实体类
 */
@ApiModel(value = "商品价格实体类")
@Data
@TableName(value = "price")
public class Price implements Serializable {
    /**
     * 商品id
     */
    @TableId(value = "product_id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "商品id")
    private String productId;

    /**
     * 商品原价,单位-元保留两位小数
     */
    @TableField(value = "original_price")
    @ApiModelProperty(value = "商品原价,单位-元保留两位小数")
    private BigDecimal originalPrice;

    /**
     * 商品促销价,单位-元保留两位小数
     */
    @TableField(value = "promote_price")
    @ApiModelProperty(value = "商品促销价,单位-元保留两位小数")
    private BigDecimal promotePrice;

    /**
     * 商品成本价,单位-元保留两位小数
     */
    @TableField(value = "cost_price")
    @ApiModelProperty(value = "商品成本价,单位-元保留两位小数")
    private BigDecimal costPrice;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    /**
     * 逻辑删除(1、删除，0、未删除)
     */
    @TableField(value = "is_delete")
    @ApiModelProperty(value = "逻辑删除(1、删除，0、未删除)")
    private Integer isDelete;

    private static final long serialVersionUID = 1L;

    public static final String PRODUCT_ID = "product_id";
    public static final String ORIGINAL_PRICE = "original_price";
    public static final String PROMOTE_PRICE = "promote_price";
    public static final String COST_PRICE = "cost_price";
    public static final String IS_DELETE = "is_delete";
}