package com.lala.admin.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 购物车实体类
 */
@ApiModel(value = "购物车实体类")
@Data
@TableName(value = "cart")
public class Cart implements Serializable {
    /**
     * 购物车id
     */
    @TableId(value = "cart_id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "购物车id")
    private String cartId;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value = "用户id")
    private String userId;

    /**
     * 商品id
     */
    @TableField(value = "goods_id")
    @ApiModelProperty(value = "商品id")
    private String goodsId;

    /**
     * 商品数量
     */
    @TableField(value = "`number`")
    @ApiModelProperty(value = "商品数量")
    private Long number;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    /**
     * 逻辑删除(1、删除，0、未删除)
     */
    @TableField(value = "is_delete")
    @ApiModelProperty(value = "逻辑删除(1、删除，0、未删除)")
    private Integer isDelete;

    private static final long serialVersionUID = 1L;

    public static final String CART_ID = "cart_id";
    public static final String USER_ID = "user_id";
    public static final String GOODS_ID = "goods_id";
    public static final String NUMBER = "number";
    public static final String CREATE_TIME = "create_time";
    public static final String UPDATE_TIME = "update_time";
    public static final String IS_DELETE = "is_delete";
}