package com.lala.admin.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 管理员实体类
 */
@ApiModel(value = "管理员实体类")
@Data
@TableName(value = "`admin`")
public class Admin implements Serializable {
    /**
     * 管理员id
     */
    @TableId(value = "admin_id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "管理员id")
    private String adminId;

    /**
     * 管理员名字
     */
    @TableField(value = "admin_name")
    @ApiModelProperty(value = "管理员名字")
    private String adminName;

    /**
     * 管理员密码
     */
    @TableField(value = "admin_password")
    @ApiModelProperty(value = "管理员密码")
    private String adminPassword;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    /**
     * 逻辑删除(1、删除，0、未删除)
     */
    @TableField(value = "is_delete")
    @ApiModelProperty(value = "逻辑删除(1、删除，0、未删除)")
    private Integer isDelete;

    private static final long serialVersionUID = 1L;

    public static final String ADMIN_ID = "admin_id";
    public static final String ADMIN_NAME = "admin_name";
    public static final String ADMIN_PASSWORD = "admin_password";
    public static final String CREATE_TIME = "create_time";
    public static final String UPDATE_TIME = "update_time";
    public static final String IS_DELETE = "is_delete";
}