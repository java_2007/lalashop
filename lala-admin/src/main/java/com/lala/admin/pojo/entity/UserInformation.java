package com.lala.admin.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

import lombok.Data;

/**
 * 用户详细信息实体类
 */
@ApiModel(value = "用户详细信息实体类")
@Data
@TableName(value = "user_information")
public class UserInformation implements Serializable {
    /**
     * 用户ID
     */
    @TableId(value = "user_id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "用户ID")
    private String userId;

    /**
     * 用户性别
     */
    @TableField(value = "sex")
    @ApiModelProperty(value = "用户性别")
    private String sex;

    /**
     * 用户邮箱
     */
    @TableField(value = "email")
    @ApiModelProperty(value = "用户邮箱")
    private String email;

    /**
     * 用户电话
     */
    @TableField(value = "iphone")
    @ApiModelProperty(value = "用户电话")
    private Long iphone;

    /**
     * 用户头像的地址
     */
    @TableField(value = "user_pic")
    @ApiModelProperty(value = "用户头像的地址")
    private String userPic;

    /**
     * vip用户 0为普通 1为vip 2为svip
     */
    @TableField(value = "vip")
    @ApiModelProperty(value = "vip用户 0为普通 1为vip 2为svip")
    private Integer vip;

    /**
     * 收获地址
     */
    @TableField(value = "address")
    @ApiModelProperty(value = "收获地址")
    private String address;

    private static final long serialVersionUID = 1L;

    public static final String USER_ID = "user_id";
    public static final String SEX = "sex";
    public static final String EMAIL = "email";
    public static final String IPHONE = "iphone";
    public static final String USER_PIC = "user_pic";
    public static final String VIP = "vip";
    public static final String ADDRESS = "address";
}