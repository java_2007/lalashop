package com.lala.payorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lala.payorder.entity.Order;

public interface OrderMapper extends BaseMapper<Order> {
}