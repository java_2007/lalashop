package com.lala.payorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lala.payorder.entity.Product;
import org.apache.ibatis.annotations.Param;

public interface ProductMapper extends BaseMapper<Product> {

    int updateByIdAndStock(@Param("product") Product product);
}