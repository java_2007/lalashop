package com.lala.payorder;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 *
 * DTO 可以控制层入口参数  业务层的入口参  包装前端需要的信息类
 *
 */
@SpringBootApplication(scanBasePackages = "com.lala.payorder")
@MapperScan("com.lala.payorder.mapper")
public class PayOrder {
    public static void main(String[] args) {
        SpringApplication.run(PayOrder.class,args);
    }
}
