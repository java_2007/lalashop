package com.lala.payorder.service.impl;

import com.lala.common.exception.ServiceException;
import com.lala.common.vo.ResponseCode;
import com.lala.common.vo.ResponseResult;
import com.lala.payorder.common.dto.OrderRequestParams;
import com.lala.payorder.common.dto.ProductRequestParams;
import com.lala.payorder.common.utils.OrderUtils;
import com.lala.payorder.entity.Address;
import com.lala.payorder.entity.Order;
import com.lala.payorder.entity.Product;
import com.lala.payorder.mapper.AddressMapper;
import com.lala.payorder.mapper.CartMapper;
import com.lala.payorder.mapper.OrderMapper;
import com.lala.payorder.mapper.ProductMapper;
import com.lala.payorder.service.OrderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Resource
    OrderMapper orderMapper;
    @Resource
    ProductMapper productMapper;
    @Resource
    AddressMapper addressMapper;
    @Resource
    CartMapper cartMapper;

    //事务，@Transactional，不需要任何的配置，直接使用,下面的配置表示有异常就回滚
    @Override
    @Transactional(rollbackFor = Exception.class, timeout = 3000)
    public ResponseResult<ResponseCode> create(OrderRequestParams orderRequestParams) {
        //通过地址ID只有一个
        String address = orderRequestParams.getDetail();
        //调用mapper层，获取地址，收件人，收件人号码
        Address addressInformation = addressMapper.selectById(address);
        //先查后改不报错

        //创建Order对象
        Order order = new Order();

        //订单号赋值
        order.setOrderId(OrderUtils.getOrderCode());
        //用户ID
        order.setUserId(addressInformation.getUserId());
        //订单总金额
        order.setMoney(orderRequestParams.getSum());
        //收货地址ID
        order.setAddressId(orderRequestParams.getDetail());
        //更改状态为5，表示已支付、
        order.setStatus(5);
        //创建时间,通过handle配置和注解上面的配合自动创建时间
        //order.setCreateTime(new Date());

        //判断库存是否足够
        List<ProductRequestParams> productRequestParams = orderRequestParams.getProductRequestParams();
        //遍历集合，获取每一个商品id，查询并返回每一个id的信息

        productRequestParams.forEach(productRequestParam ->{
            Product product = productMapper.selectById(productRequestParam.getProductId());
            if(product.getStock()<productRequestParam.getNum() || product.getStock() == 0){
                throw new ServiceException(ResponseCode.PRODUCT_STOCK_OUT);
            }
        });

        //商品已下架
        productRequestParams.forEach(productRequestParam ->{
            Product product = productMapper.selectById(productRequestParam.getProductId());
            if(product.getStatus() == 2){
                throw new ServiceException(ResponseCode.ORDER_PRODUCT_OUT);
            }
        });


        //库存足够，创建订单.插入订单表，
        int state = orderMapper.insert(order);
        if (state != 1){
            throw new ServiceException(ResponseCode.ERROR);
        }

        //减库存，增销量
        productRequestParams.forEach(productRequestParam -> {
            Product product = productMapper.selectById(productRequestParam.getProductId());
            if (product != null){
                //新库存数
                product.setStock(product.getStock() - productRequestParam.getNum());
                //增销量
                product.setSalesNum(product.getSalesNum() + productRequestParam.getNum());
                //调用mapper,更新库存和销量
                int i = productMapper.updateByIdAndStock(product);
                if (i != 1){
                    throw new ServiceException(ResponseCode.ERROR);
                }
            }else {
                throw new ServiceException(ResponseCode.ERROR);
            }
        });

        //从购物车删除
        productRequestParams.forEach(productRequestParam ->{
            int j = cartMapper.updateId(productRequestParam.getProductId());
            if (j != 1){
                throw new ServiceException(ResponseCode.ERROR);
            }
        });

        return ResponseResult.success(ResponseCode.SUCCESS);
    }
}
