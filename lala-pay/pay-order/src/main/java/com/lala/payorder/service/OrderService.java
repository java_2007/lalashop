package com.lala.payorder.service;

import com.lala.common.vo.ResponseCode;
import com.lala.common.vo.ResponseResult;
import com.lala.payorder.common.dto.OrderRequestParams;

public interface OrderService {
    ResponseResult<ResponseCode> create(OrderRequestParams orderRequestParams);
}
