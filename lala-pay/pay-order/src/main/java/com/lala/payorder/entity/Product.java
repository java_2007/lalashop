package com.lala.payorder.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
    * 商品表
    */
@Data
@TableName(value = "product")
public class Product implements Serializable {
    /**
     * 商品id
     */
    @TableId(value = "product_id", type = IdType.INPUT)
    private String productId;

    /**
     * 一级分类id
     */
    @TableField(value = "category_1st")
    private String category1st;

    /**
     * 二级分类id
     */
    @TableField(value = "category_2nd")
    private String category2nd;

    /**
     * 三级分类id
     */
    @TableField(value = "category_3rd")
    private String category3rd;

    /**
     * 商品名称
     */
    @TableField(value = "product_name")
    private String productName;

    /**
     * 商品副标题
     */
    @TableField(value = "subtitle")
    private String subtitle;

    /**
     * 产品主图,url相对地址
     */
    @TableField(value = "image_url")
    private String imageUrl;

    /**
     * 商品详情
     */
    @TableField(value = "detail")
    private String detail;

    /**
     * 库存数量
     */
    @TableField(value = "stock")
    private Long stock;

    /**
     * 销量
     */
    @TableField(value = "sales_num")
    private Integer salesNum;

    /**
     * 商品状态.1-在售 2-下架
     */
    @TableField(value = "`status`")
    private Integer status;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 逻辑删除(1、删除，0、未删除)
     */
    @TableField(value = "is_delete")
    private Integer isDelete;

    @TableField(value = "`version`")
    private Integer version;

    private static final long serialVersionUID = 1L;

    public static final String COL_PRODUCT_ID = "product_id";

    public static final String COL_CATEGORY_1ST = "category_1st";

    public static final String COL_CATEGORY_2ND = "category_2nd";

    public static final String COL_CATEGORY_3RD = "category_3rd";

    public static final String COL_PRODUCT_NAME = "product_name";

    public static final String COL_SUBTITLE = "subtitle";

    public static final String COL_IMAGE_URL = "image_url";

    public static final String COL_DETAIL = "detail";

    public static final String COL_STOCK = "stock";

    public static final String COL_SALES_NUM = "sales_num";

    public static final String COL_STATUS = "status";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_TIME = "update_time";

    public static final String COL_IS_DELETE = "is_delete";
}