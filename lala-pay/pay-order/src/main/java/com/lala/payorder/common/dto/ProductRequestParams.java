package com.lala.payorder.common.dto;

import lombok.Data;

@Data
public class ProductRequestParams {
    //商品ID
    private String productId;
    //商品数量
    private Integer num;

}
