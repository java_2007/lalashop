package com.lala.payorder.common.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 考虑性能问题，并发在前端处理，压力分散到客户端
 */
@Data
public class OrderRequestParams {
    //收货地址ID
    private String detail;

    //购买的商品信息
    private List<ProductRequestParams> productRequestParams;

    //优惠券,暂时只一张优惠券，不搞叠加,多选list
    private Integer couponId;

    //商品总金额
    private BigDecimal sum;

}
