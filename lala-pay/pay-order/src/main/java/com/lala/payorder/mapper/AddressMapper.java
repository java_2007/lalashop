package com.lala.payorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lala.payorder.entity.Address;

public interface AddressMapper extends BaseMapper<Address> {
}