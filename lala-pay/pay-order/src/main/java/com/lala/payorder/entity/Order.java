package com.lala.payorder.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

/**
    * 订单表
    */
@Data
@TableName(value = "`order`")
public class Order implements Serializable {
    /**
     * 订单编号
     */
    @TableId(value = "order_id", type = IdType.INPUT)
    private String orderId;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    private String userId;

    /**
     * 订单总金额
     */
    @TableField(value = "money")
    private BigDecimal money;

    /**
     * 收货地址id
     */
    @TableField(value = "address_id")
    private String addressId;

    /**
     * 订单状态：0未支付，1支付失败，2支付成功,3发货，4到货，5评价
     */
    @TableField(value = "`status`")
    private Integer status;

    /**
     * 创建时间 fill = FieldFill.INSERT
     */
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 修改时间 FieldFill.INSERT_UPDATE
     */
    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 收货人
     */
    @TableField(value = "phone")
    private Long phone;

    /**
     * 逻辑删除(1、删除，0、未删除)
     */
    @TableField(value = "is_delete")
    private Integer isDelete;

    private static final long serialVersionUID = 1L;

    public static final String COL_ORDER_ID = "order_id";

    public static final String COL_USER_ID = "user_id";

    public static final String COL_MONEY = "money";

    public static final String COL_ADDRESS_ID = "address_id";

    public static final String COL_STATUS = "status";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_TIME = "update_time";

    public static final String COL_PHONE = "phone";

    public static final String COL_IS_DELETE = "is_delete";
}