package com.lala.payorder.controller;

import com.lala.common.vo.ResponseCode;
import com.lala.common.vo.ResponseResult;
import com.lala.payorder.common.dto.OrderRequestParams;
import com.lala.payorder.service.OrderService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


@RestController
public class OrderController {
    @Resource
    OrderService orderService;

    /**
     * 创建订单
     *
     */
    @PostMapping("/order")
    public ResponseResult<ResponseCode> create(OrderRequestParams orderRequestParams){
        return orderService.create(orderRequestParams);
    }
}
