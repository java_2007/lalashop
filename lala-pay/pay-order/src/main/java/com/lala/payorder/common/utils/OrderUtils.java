package com.lala.payorder.common.utils;

import cn.hutool.core.date.DateUtil;

import java.util.Date;
import java.util.Random;

public class OrderUtils {
    //时间格式
    public static final String DATE_FORMAT = "yyyyMMddHHmmss";
    //前缀
    public static final String ORDER_PREFIX = "lala";

    public static String getOrderCode(){
        //测试Random可不可以用
        Random random = new Random();
        //前面String后面是可变参数
        return String.format("%s%s%s", ORDER_PREFIX,
                DateUtil.format(new Date(), DATE_FORMAT),random.nextInt(6)
        );
    }
}
