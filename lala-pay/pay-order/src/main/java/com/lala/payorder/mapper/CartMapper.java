package com.lala.payorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lala.payorder.entity.Cart;
import org.apache.ibatis.annotations.Param;

public interface CartMapper extends BaseMapper<Cart> {

    int updateId(String productId);

    String selectAAA(@Param("name") String name);
}