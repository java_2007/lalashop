package com.lala.alipay.config;

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

@Configuration
public class AlipayConfiguration {
    //第一步导入类
    @Resource
    AlipayConfigurationProperties properties;

    //第二步就是官方文档上的第一步，实例化客户端，并把类里面的数值传递进去
    @Bean
    public AlipayClient alipayClient() {
        return new DefaultAlipayClient(
                  properties.getServerUrl()
                , properties.getAppId()
                , properties.getPrivateKey()
                , properties.getFormat()
                , properties.getCharset()
                , properties.getAlipayPublicKey()
                , properties.getSignType()
        );
    }
}
