package com.lala.alipay.service.impl;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.AlipayResponse;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.lala.alipay.common.PayRequestParams;
import com.lala.alipay.config.AlipayConfigurationProperties;
import com.lala.alipay.service.AliPayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Slf4j
@Service
public class AliPayServiceImpl implements AliPayService {
    //实例化客户端，文档第一步
    @Resource
    AlipayClient Client;
    //config内的类，有配置信息
    @Resource
    AlipayConfigurationProperties properties;

    @Override
    public String payApp(PayRequestParams payRequestParams) throws AlipayApiException {
        try {
            AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
            AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
            model.setSubject(payRequestParams.getSubject());

            model.setTotalAmount(payRequestParams.getTotalAmount());
            model.setOutTradeNo(payRequestParams.getTradeNo());
            // 支模模式，网页支付
            model.setProductCode(properties.getProductCode());
            // 支付的超时时间
            model.setTimeoutExpress(properties.getTimeoutExpress());
            request.setBizModel(model);
            request.setNotifyUrl(properties.getNotifyUrl());
            request.setReturnUrl(properties.getReturnUrl());
            // 就是文档上，这里和普通的接口调用不同，使用的是sdkExecute
            AlipayResponse alipayResponse = Client.sdkExecute(request);
            if (alipayResponse.isSuccess()) {
                // 支付连接
                String body = alipayResponse.getBody();
                // 这里是日志打印
                log.info(body);
                return body;
            }
        } catch (RuntimeException e) {
            throw new RuntimeException("生成支付链接失败");
        }
        return "链接失败";
    }
}
