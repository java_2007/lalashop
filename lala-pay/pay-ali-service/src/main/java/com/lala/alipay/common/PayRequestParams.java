package com.lala.alipay.common;

import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class PayRequestParams {
    //订单号
    @ApiParam("订单号")
    private String tradeNo;
    // 订单的名称
    private String subject;
    /**
     * 支付的总金额
     */
    private String totalAmount;

}
