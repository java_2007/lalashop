package com.lala.alipay.service;

import com.alipay.api.AlipayApiException;
import com.lala.alipay.common.PayRequestParams;

public interface AliPayService {
    String payApp(PayRequestParams payRequestParams) throws AlipayApiException;
}
