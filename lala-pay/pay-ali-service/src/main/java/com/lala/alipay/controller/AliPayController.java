package com.lala.alipay.controller;

import com.alipay.api.AlipayApiException;
import com.lala.alipay.common.PayRequestParams;
import com.lala.alipay.service.AliPayService;
import com.lala.common.vo.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 微服务 控制层传递json给控制层
 */
@Api(tags = "alipay")
@RestController
@RequestMapping("/ali")
public class AliPayController {
    //Service层
    @Resource
    AliPayService payService;

    /**
     * 前后端分离的支付方式
     * 内部之间的调用，可以通过内网IP调用服务
     * PayRequestParams 前端发送包含订单号等信息的类
     * @return
     */
    @ApiOperation("alipay接口")
    @PostMapping("/")
    public ResponseResult<String> payApp(@RequestBody PayRequestParams payRequestParams) throws AlipayApiException {
        return ResponseResult.success(payService.payApp(payRequestParams));
    }
}
