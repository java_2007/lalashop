package com.lala.alipay.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 *  需要配置信息用@ConfigurationProperties(prefix = "pay.ali")
 */
@Component
@ConfigurationProperties(prefix = "pay.ali")
@Data
public class AlipayConfigurationProperties {
    private String serverUrl;
    private String appId;
    private String privateKey;
    private String format;
    private String charset;
    private String alipayPublicKey;
    private String signType;
    private String timeoutExpress;
    private String productCode;
    private String returnUrl;
    private String notifyUrl;
}
