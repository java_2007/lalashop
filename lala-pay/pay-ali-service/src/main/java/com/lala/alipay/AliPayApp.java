package com.lala.alipay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.net.HttpURLConnection;

/**
 * 原生
 * HttpURLConnection
 * HttpClient
 * OkHttp
 */
@SpringBootApplication
public class AliPayApp {
    public static void main(String[] args) {
        SpringApplication.run(AliPayApp.class, args);

    }
}
