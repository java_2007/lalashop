package com.lala.pay.controller;

import com.alipay.api.AlipayApiException;
import com.lala.common.vo.ResponseResult;
import com.lala.pay.common.PayRequestParams;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 支付连接
 * 服务跟服务(同步消息)
 * 服务跟服务(异步消息) MQ
 *
 */
@RestController
@RequestMapping("/pay")
public class PayController {


    /**
     * @param type 支付方式  1 表示支付  2 表示 wx支付 3 表示银联支付
     * @return
     */
    @PostMapping("/")
    public ResponseResult<String> pay(int type, PayRequestParams payRequestParams) throws AlipayApiException {
        if (type == 1){
            return null;
        }
        return ResponseResult.error();
    }

}
